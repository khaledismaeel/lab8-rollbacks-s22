import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=postgres user=postgres")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
get_sum_of_inventory_items = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
buy_or_increment_inventory = "INSERT INTO Inventory VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = inventory.amount + %(amount)s"



def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try:
                cur.execute(get_sum_of_inventory_items, obj)
                current_sum_of_inventory_items = cur.fetchone()[0]
                if (current_sum_of_inventory_items is not None and current_sum_of_inventory_items >= 100):
                    raise Exception("The user is not allowed to buy more items.")
                cur.execute(buy_or_increment_inventory, obj)
            except psycopg2.errors.CheckViolation as e:
                raise Exception()

            conn.commit()

if __name__ == '__main__':
  buy_product('Alice', 'marshmello', 1)
